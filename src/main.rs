// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env::args, fs, io, path::Path};
fn main() {
    let args: Vec<String> = args().collect();
    if args.len() == 1 {
        panic!("No file to read.")
    }
    println!("XED v{}", env!("CARGO_PKG_VERSION"));
    start_editor(get_file_contents(args[1].to_string()), args[1].to_string());
}
fn get_file_contents(path: String) -> String {
    if Path::new(&path[..]).exists() {
        fs::read_to_string(path).unwrap()
    } else {
        fs::write(&path[..], "").expect("Could not create file, you may want to run `touch FILE` to create the file, and then run xed.");
        String::from("")
    }
}
pub fn start_editor(file: String, path: String) {
    let mut xed = (true, file, 0); // xed.0 is the mode, xed.1 is the file, and xed.2 is the line
    let mut last_input = "".to_string();
    loop {
        let input = read_input();
        if xed.0 {
            if &last_input[..] == "" {
                match &input[..] {
                    | "q" => break,               // q for quit
                    | "s" => save(&xed.1, &path), // S for save
                    | _ => {}
                }
            }
            match &input[..] {
                | "c" => {
                    let mut input2 = read_input().replace(".", "\n");
                    if input2.is_empty() {
                        input2 = format!("1\n{}", xed.1.lines().count());
                    }
                    let mut input = input2.lines();
                    for i in input.next().unwrap().parse::<usize>().unwrap() ..= input.next().unwrap().parse::<usize>().unwrap() {
                        println!("{}", get_line(&(i as usize), &xed.1));
                    }
                }, // c for cat
                | "r" => println!("{}", get_line(&(read_input().parse::<usize>().expect("Need u32")), &xed.1)), // R for read
                | "l" => {
                    xed.2 = read_input().parse::<usize>().expect("Need u32");
                    println!("\n\x1b[2m{}\x1b[0m\x1b[1A\x1b[\x35 q", get_line(&xed.2, &xed.1));
                    xed.0 = false;
                }, // l for line
                | "a" => {
                    let line = read_input().parse::<usize>().expect("Expected usize.");
                    xed.1 = change_line(line, &xed.1, &format!("{}\n", get_line(&line, &xed.1)));
                    xed.2 = line + 1;
                    println!("\n{}{}", "\x1b[1A", "\x1b[\x35 q");
                    xed.0 = false;
                },
                | "e" => {
                    let line = read_input().parse::<usize>().expect("Expected usize.");
                    let mut lines: Vec<&str> = xed.1.split('\n').collect();
                    lines.remove(line - 1);
                    xed.1 = lines.join("\n");
                },
                | _ => ()
            }
            last_input = input;
        } else {
            xed.1 = change_line(xed.2, &xed.1, &input);
            xed.0 = true;
            println!("\x1b[\x31 q");
            last_input = input;
        }
    }
    print!("\x1b[ q");
}
fn read_input() -> String {
    let mut line: String = String::new();
    io::stdin().read_line(&mut line).expect("Failed to read line");
    line.replace("\n", "")
}
fn get_line(num: &usize, file: &str) -> String {
    let lines: Vec<&str> = file.split('\n').collect();
    lines[num - 1].to_string()
}
fn change_line(num: usize, file: &str, input: &str) -> String {
    let mut lines: Vec<&str> = file.split('\n').collect();
    lines[num - 1] = &input[..];
    let file2: String = lines.join("\n");
    file2
}
fn save(input: &str, path: &str) {
    match fs::write(&path, input) {
        | Ok(_) => (),
        | Err(e) => eprintln!("Could not write to file, {}: {}", &path, e)
    }
}
